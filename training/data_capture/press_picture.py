from picamera import PiCamera
import time


def initialize_camera():
    cam = PiCamera()
    cam.resolution = (720, 720)
    return cam


def take_picture(cam):
    stamp = time.strftime("%Y%m%d-%H%M%S")
    cam.capture("/home/pi/Pictures/"  + stamp + '.png')
    print("image captured")
    time.sleep(0.2)


def pic_by_pin(pin_num, cam):
    from RPi import GPIO as GPIO

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin_num, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    while True:
        input_state = GPIO.input(15)
        if not input_state:
            take_picture(cam)


def gui_pic(cam):
    from tkinter import Tk, Frame, Button
    root = Tk()
    root.wm_title("Image Capture")
    frame = Frame(root, width=100, height=100)
    frame.grid(row=0, column=0, padx=0, pady=0)
    button = Button(frame, text="Capture", command= lambda: take_picture(cam))
    button.grid(row=0, column=0, padx=0, pady=0)
    root.mainloop()


def main():
    using_pins = False
    cam = initialize_camera()
    cam.start_preview()
    time.sleep(2)
    print('Camera ready')
    if using_pins:
        pic_by_pin(15, cam)
    else:
        gui_pic(cam)
    cam.stop_preview()

main()
